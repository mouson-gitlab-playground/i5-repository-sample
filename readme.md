# How to use

1. git clone https://gitlab.com/mouson/i5-repository-sample.git sample
2. cd sample
3. composer install
4. cp .env.example .env
5. ./artisan key:generate --ansi
6. touch database/database.sqlite
7. setting .env file
    change DB_CONNECTION=sqlite
    delete other DB_*
8. ./artisan migrate
9. ./artisan db:seed
10. ./artisan serve

## Code

```=
app/Http/Controllers/Controller.php
app/Repositories/PostRepository.php
app/Repositories/PostRepositoryEloquent.php
```

`findByField` is implement by i5-repository BaseRepository, now PostRepository extended BaseRepository and overwrite it. 

## change controller dependence to interface

1. ./artisan make:bindings Post
2. modify config/app.php
   add App\Providers\RepositoryServiceProvider::class,
3. change controller dependence to PostRepository interface.
