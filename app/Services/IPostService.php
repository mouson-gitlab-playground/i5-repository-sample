<?php

namespace App\Services;

interface IPostService
{
    public function getPostDetail($postId);
}
