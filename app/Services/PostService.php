<?php

namespace App\Services;

use App\Repositories\PostRepository;

class PostService implements IPostService
{
    /**
     * @var $postRepository PostRepository
     */
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getPostDetail($postId)
    {
        $result = $this->postRepository
                       ->with('authors')
                       ->findByField('id', $postId);
        return $result;
    }
}
