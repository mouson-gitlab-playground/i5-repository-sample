<?php

namespace App\Http\Controllers;

use App\Services\IPostService;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @var IPostService
     */
    protected $postService;

    public function __construct(IPostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        return $this->postService->getPostDetail(2);
    }

}
