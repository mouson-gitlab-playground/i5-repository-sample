<?php

namespace Tests\Unit;

use App\Repositories\PostRepository;
use App\Services\IPostService;
use Mockery as m;
use Tests\TestCase;

class PostServiceTest extends TestCase
{
    /**
     * @var m::Spy|PostRepository
     */
    protected $spyPostRepository;
    /**
     * @var IPostService
     */
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->spyPostRepository = $this->initSpy(PostRepository::class);
        $this->service = app(IPostService::class);
    }

    /**
     * Initial Spy Object
     *
     * @param string $class
     *
     * @return m\Mock
     */
    public function initSpy(string $class)
    {
        $spy = m::spy($class);
        $this->app->instance($class, $spy);

        return $spy;
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetDetail()
    {
        $postId = 1;
        $detail = collect();
        $detail->push([
            'title' => 'TITLE',
        ]);

        $this->spyPostRepository
             ->shouldReceive('with->findByField')
             ->andReturn($detail);

        $actual = $this->service->getPostDetail($postId);
        $this->assertEquals($detail, $actual);

    }
}
